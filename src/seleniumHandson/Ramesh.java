package seleniumHandson;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Ramesh {
	
	public static void main(String[] args)
	
		 {
			 
			WebDriver wb = new FirefoxDriver();
			wb.manage().window().maximize();
			wb.get("https://www.google.com");
			
			//we have used locator as name
			wb.findElement(By.name("q")).sendKeys("Ramesh");
			System.out.println("Entering the Text Ramesh");
			
			// we have used locator as xpath
			wb.findElement(By.xpath("//*[@id='sblsbb']/button")).click();
			System.out.println("Clicked on Search Button");
					
			wb.quit();
		
		
	}
		
}

